package ru.trifonov.tm.entity;

import java.util.Date;

public final class Project {
    private String name;
    private String id;
    private String userId;
    private String description;
    private Date beginDate;
    private Date endDate;

    public Project() {
    }

    public Project(String name, String id, String userId, String description, Date beginDate, Date endDate) {
        this.name = name;
        this.id = id;
        this.userId = userId;
        this.description = description;
        this.beginDate = beginDate;
        this.endDate = endDate;
    }

    @Override
    public String toString() {
        return " ID " + id +
                "  NAME " + name +
                "  DESCRIPTION " + description +
                "  PROJECT START DATE " + beginDate +
                "  PROJECT FINISH DATE " + endDate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getBeginDate() {
        return beginDate;
    }

    public void setBeginDate(Date beginDate) {
        this.beginDate = beginDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }
}