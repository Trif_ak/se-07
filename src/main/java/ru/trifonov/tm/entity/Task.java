package ru.trifonov.tm.entity;

import java.util.Date;

public final class Task {
    private String name;
    private String id;
    private String projectId;
    private String userId;
    private String description;
    private Date beginDate;
    private Date endDate;

    public Task() {
    }

    public Task(String name, String id, String projectId, String userId, String description, Date beginDate, Date endDate) {
        this.name = name;
        this.id = id;
        this.projectId = projectId;
        this.userId = userId;
        this.description = description;
        this.beginDate = beginDate;
        this.endDate = endDate;
    }

    @Override
    public String toString() {
        return " ID " + id +
                "  NAME " + name +
                "  DESCRIPTION " + description +
                "  PROJECT START DATE " + beginDate +
                "  PROJECT FINISH DATE " + endDate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Date getBeginDate() {
        return beginDate;
    }

    public void setBeginDate(Date beginDate) {
        this.beginDate = beginDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
