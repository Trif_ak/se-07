package ru.trifonov.tm.enumerate;

public enum RoleType {
    ADMIN("admin"), REGULAR_USER("user");
    private String type;

    RoleType(String type) {
        this.type = type;
    }

    public String getRoleType() {
        return type;
    }
}
