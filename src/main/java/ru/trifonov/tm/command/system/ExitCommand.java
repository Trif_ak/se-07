package ru.trifonov.tm.command.system;

import ru.trifonov.tm.command.AbstractCommand;
import ru.trifonov.tm.enumerate.RoleType;

public final class ExitCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "exit";
    }

    @Override
    public String getDescription() {
        return ": program exit";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[PROGRAM EXIT]");
        System.exit(0);
    }

    @Override
    public RoleType[] roleType() {
        return null;
    }
}
