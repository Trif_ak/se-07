package ru.trifonov.tm.command.system;

import com.jcabi.manifests.Manifests;
import ru.trifonov.tm.command.AbstractCommand;
import ru.trifonov.tm.enumerate.RoleType;

public final class AboutCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "about";
    }

    @Override
    public String getDescription() {
        return ": return information of this application";
    }

    @Override
    public void execute() throws Exception {
        System.out.println(
                " Developer: " + Manifests.read("Developer") +
                "\n Version: " + Manifests.read("Version") +
                "\n BuildNumber" + Manifests.read("BuildNumber"));
    }

    @Override
    public RoleType[] roleType() {
        return null;
    }
}
