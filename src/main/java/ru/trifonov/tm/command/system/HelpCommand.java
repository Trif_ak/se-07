package ru.trifonov.tm.command.system;

import ru.trifonov.tm.command.AbstractCommand;
import ru.trifonov.tm.enumerate.RoleType;

public final class HelpCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "help";
    }

    @Override
    public String getDescription() {
        return ": show all COMMANDS";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[SHOW ALL COMMANDS]");
        for (final AbstractCommand command : serviceLocator.getCommands().values())
            System.out.println(command.getName() + command.getDescription());
    }

    @Override
    public RoleType[] roleType() {
        return null;
    }
}
