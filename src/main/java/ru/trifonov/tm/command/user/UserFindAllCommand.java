package ru.trifonov.tm.command.user;

import ru.trifonov.tm.command.AbstractCommand;
import ru.trifonov.tm.entity.User;
import ru.trifonov.tm.enumerate.RoleType;

import java.util.Collection;
import java.util.List;

public final class UserFindAllCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "user-findAll";
    }

    @Override
    public String getDescription() {
        return ": return all users";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[FIND ALL USERS]");
        final Collection<User> inputList = serviceLocator.getUserService().findAll();
        for (final User user : inputList) {
            System.out.println(user);
        }
        System.out.println("[OK]");
    }

    @Override
    public RoleType[] roleType() {
        return new RoleType[] {RoleType.ADMIN};

    }
}
