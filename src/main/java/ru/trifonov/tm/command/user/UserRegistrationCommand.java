package ru.trifonov.tm.command.user;

import ru.trifonov.tm.command.AbstractCommand;
import ru.trifonov.tm.enumerate.RoleType;

public final class UserRegistrationCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "user-reg";
    }

    @Override
    public String getDescription() {
        return ": registration new user";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[USER REGISTRATION]");
        System.out.println("Enter your LOGIN");
        final String login = serviceLocator.getInCommand().nextLine();
        System.out.println("Enter your PASSWORD");
        final String password = serviceLocator.getInCommand().nextLine();
        System.out.println("USER " + serviceLocator.getUserService().registrationUser(login, password) + " is registered");
        System.out.println("[OK]");
    }

    @Override
    public RoleType[] roleType() {
        return null;
    }
}
