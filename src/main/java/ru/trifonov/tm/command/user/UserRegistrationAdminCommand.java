package ru.trifonov.tm.command.user;

import ru.trifonov.tm.command.AbstractCommand;
import ru.trifonov.tm.enumerate.RoleType;

public final class UserRegistrationAdminCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "user-regAdmin";
    }

    @Override
    public String getDescription() {
        return ": registration new admin";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[USER REGISTRATION]");
        System.out.println("Enter your LOGIN");
        final String login = serviceLocator.getInCommand().nextLine();
        System.out.println("Enter your PASSWORD");
        final String password = serviceLocator.getInCommand().nextLine();
        System.out.println("ADMIN " + serviceLocator.getUserService().registrationAdmin(login, password) + " is registered");
        System.out.println("[OK]");
    }

    @Override
    public RoleType[] roleType() {
        return new RoleType[] {RoleType.ADMIN};
    }
}
