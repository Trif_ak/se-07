package ru.trifonov.tm.command.user;

import ru.trifonov.tm.command.AbstractCommand;
import ru.trifonov.tm.enumerate.RoleType;

public final class UserEndOfSessionCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "user-exit";
    }

    @Override
    public String getDescription() {
        return ": end of user session";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[USER LOGOUT]");
        serviceLocator.setCurrentUser(null);
        System.out.println("[OK]");
    }

    @Override
    public RoleType[] roleType() {
        return new RoleType[] {RoleType.REGULAR_USER, RoleType.ADMIN};
    }
}
