package ru.trifonov.tm.command.user;

import ru.trifonov.tm.command.AbstractCommand;
import ru.trifonov.tm.enumerate.RoleType;

public final class UserEditProfileCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "user-edit";
    }

    @Override
    public String getDescription() {
        return ": edit user";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[EDIT REGULAR_USER]");
        final String id = serviceLocator.getCurrentUserID();
        System.out.println("Enter new login");
        final String login = serviceLocator.getInCommand().nextLine();
        System.out.println("Enter new password");
        final String password = serviceLocator.getInCommand().nextLine();
        serviceLocator.getUserService().update(id, login, password, serviceLocator.getCurrentUser().getRoleType());
        System.out.println("[OK]");
    }

    @Override
    public RoleType[] roleType() {
        return new RoleType[] {RoleType.REGULAR_USER, RoleType.ADMIN};
    }
}
