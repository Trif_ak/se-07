package ru.trifonov.tm.command.user;

import ru.trifonov.tm.command.AbstractCommand;
import ru.trifonov.tm.enumerate.RoleType;

public final class UserRemoveAllCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "user-removeAllOfProject";
    }

    @Override
    public String getDescription() {
        return ": removeOne all users";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[REMOVE ALL USER]");
        serviceLocator.getUserService().removeAll();
        System.out.println("[OK]");
    }

    @Override
    public RoleType[] roleType() {
        return new RoleType[] {RoleType.ADMIN};

    }
}
