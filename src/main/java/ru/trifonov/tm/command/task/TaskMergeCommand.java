package ru.trifonov.tm.command.task;

import ru.trifonov.tm.command.AbstractCommand;
import ru.trifonov.tm.enumerate.RoleType;

public final class TaskMergeCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "task-merge";
    }

    @Override
    public String getDescription() {
        return ": merge select task";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[MERGE TASK]");
        System.out.println("Enter the ID of the task you want to merge");
        final String id = serviceLocator.getInCommand().nextLine();
        System.out.println("Enter new name");
        final String name = serviceLocator.getInCommand().nextLine();
        System.out.println("Enter new projectId");
        final String projectId = serviceLocator.getInCommand().nextLine();
        final String userId = serviceLocator.getCurrentUserID();
        System.out.println("Enter new description");
        final String description = serviceLocator.getInCommand().nextLine();
        System.out.println("Enter new start date task. Date format DD.MM.YYYY");
        final String beginDate = serviceLocator.getInCommand().nextLine();
        System.out.println("Enter new finish date task. Date format DD.MM.YYYY");
        final String endDate = serviceLocator.getInCommand().nextLine();
        serviceLocator.getTaskService().merge(name, id, projectId, userId, description, beginDate, endDate);
        System.out.println("[OK]");
    }

    @Override
    public RoleType[] roleType() {
        return new RoleType[] {RoleType.REGULAR_USER, RoleType.ADMIN};
    }
}
