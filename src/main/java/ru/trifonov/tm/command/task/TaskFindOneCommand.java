package ru.trifonov.tm.command.task;

import ru.trifonov.tm.command.AbstractCommand;
import ru.trifonov.tm.enumerate.RoleType;

public final class TaskFindOneCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "task-findOne";
    }

    @Override
    public String getDescription() {
        return ": return select task";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[FIND TASK]");
        System.out.println("Enter the ID of the task");
        final String id = serviceLocator.getInCommand().nextLine();
        final String userId = serviceLocator.getCurrentUserID();
        System.out.println(serviceLocator.getTaskService().findOne(id, userId));
        System.out.println("[OK]");
    }

    @Override
    public RoleType[] roleType() {
        return new RoleType[] {RoleType.REGULAR_USER, RoleType.ADMIN};

    }
}
