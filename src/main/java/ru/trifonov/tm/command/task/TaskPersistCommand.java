package ru.trifonov.tm.command.task;

import ru.trifonov.tm.command.AbstractCommand;
import ru.trifonov.tm.enumerate.RoleType;

public final class TaskPersistCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "task-persist";
    }

    @Override
    public String getDescription() {
        return ": create new task";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[PERSIST TASK]");
        System.out.println("Enter ID of project");
        final String projectId = serviceLocator.getInCommand().nextLine();
        final String userId = serviceLocator.getCurrentUserID();
        System.out.println("Enter name task");
        final String name = serviceLocator.getInCommand().nextLine();
        System.out.println("Enter description");
        final String description = serviceLocator.getInCommand().nextLine();
        System.out.println("Enter start date task. Date format DD.MM.YYYY");
        final String beginDate = serviceLocator.getInCommand().nextLine();
        System.out.println("Enter finish date task. Date format DD.MM.YYYY");
        final String endDate = serviceLocator.getInCommand().nextLine();
        serviceLocator.getTaskService().persist(name, projectId, userId, description, beginDate, endDate);
        System.out.println("[OK]");
    }

    @Override
    public RoleType[] roleType() {
        return new RoleType[] {RoleType.REGULAR_USER, RoleType.ADMIN};
    }
}
