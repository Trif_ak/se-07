package ru.trifonov.tm.bootstrap;

import ru.trifonov.tm.api.*;
import ru.trifonov.tm.entity.User;
import ru.trifonov.tm.enumerate.RoleType;
import ru.trifonov.tm.repository.*;
import ru.trifonov.tm.repository.UserRepository;
import ru.trifonov.tm.service.*;
import ru.trifonov.tm.command.AbstractCommand;

import java.util.*;

public final class Bootstrap implements ServiceLocator {
    private User currentUser = new User();
    private Collection<RoleType> roleTypes;
    private final IProjectRepository projectRepository = new ProjectRepository();
    private final ITaskRepository taskRepository = new TaskRepository();
    private final IUserRepository userRepository = new UserRepository();
    private final IProjectService projectService = new ProjectService(projectRepository);
    private final ITaskService taskService = new TaskService(taskRepository);
    private final IUserService userService = new UserService(userRepository);
    private final Scanner inCommand = new Scanner(System.in);
    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    public void init(final Class... commandsClass) throws Exception {
            if (commandsClass == null) return;
            for (final Class clazz : commandsClass) {
                registry(clazz);
            }
            start();
    }

    private void registry(final Class clazz) throws Exception {
        if (!AbstractCommand.class.isAssignableFrom(clazz)) return;
        final AbstractCommand abstractCommand = (AbstractCommand) clazz.newInstance();
        abstractCommand.setServiceLocator(this);
        final String nameCommand = abstractCommand.getName();
        final String descriptionCommand = abstractCommand.getDescription();
        if (nameCommand == null || nameCommand.trim().isEmpty()) return;
        if (descriptionCommand == null || descriptionCommand.trim().isEmpty()) return;
        commands.put(nameCommand, abstractCommand);
    }

    public void start() throws Exception {
        userService.registrationAdmin("admin", "admin");
        userService.registrationUser("user", "user");
        System.out.println("*** WELCOME TO TASK MANAGER *** \n Enter command \"help\" for watch all COMMANDS");
        while (true) {
            System.out.println("\n Enter command:");
            final AbstractCommand abstractCommand = commands.get(inCommand.nextLine());
            if (abstractCommand == null) {
                System.out.println("There is no such command");
                continue;
            }
            execute(abstractCommand, currentUser);
        }
    }

    private void execute (final AbstractCommand abstractCommand, final User currentUser) throws Exception {
        if (abstractCommand.roleType() != null) {
            roleTypes = Arrays.asList(abstractCommand.roleType());
            if (roleTypes.contains(currentUser.getRoleType())) abstractCommand.execute();
            else System.out.println("This command is not available");
        }
        else abstractCommand.execute();
    }

    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    @Override
    public IUserService getUserService() {
        return userService;
    }

    @Override
    public Scanner getInCommand() {
        return inCommand;
    }

    @Override
    public Map<String, AbstractCommand> getCommands() {
        return commands;
    }

    @Override
    public String getCurrentUserID() {
        return currentUser.getId();
    }

    @Override
    public User getCurrentUser() {
        return currentUser;
    }

    @Override
    public void setCurrentUser(User currentUser) {
        this.currentUser = currentUser;
    }
}


