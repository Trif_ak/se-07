package ru.trifonov.tm.repository;

import ru.trifonov.tm.api.IProjectRepository;
import ru.trifonov.tm.entity.*;

import java.util.*;

public final class ProjectRepository extends AbstractRepository<Project> implements IProjectRepository {
    @Override
    public void persist(final Project project) {
        entities.put(project.getId(), project);
    }

    @Override
    public void merge(final Project project) {
        if (entities.containsKey(project.getId())) {
            update(project.getName(), project.getId(), project.getUserId(), project.getDescription(), project.getBeginDate(), project.getEndDate());
        } else
            insert(project.getName(), project.getId(), project.getUserId(), project.getDescription(), project.getBeginDate(), project.getEndDate());
    }

    @Override
    public void insert(final String name, final String id, final String userId, final String description, final Date beginDate, final Date endDate) {
        final Project project = new Project(name, id, userId, description, beginDate, endDate);
        entities.put(id, project);
    }

    @Override
    public void update(final String name, final String id, final String userId, final String description, final Date beginDate, final Date endDate) {
        final Project project = new Project(name, id, userId, description, beginDate, endDate);
        entities.put(id, project);
    }

    @Override
    public Project findOne(final String id, final String userId) {
        Project project = entities.get(id);
        if (project == null) throw new NullPointerException();
        if (!project.getUserId().equals(userId)) throw new IllegalStateException();
        return project;
    }

    @Override
    public Collection<Project> findAll(final String userId) {
        final Collection<Project> output = new ArrayList<>();
        for (Map.Entry<String, Project> project : entities.entrySet()) {
            if (project.getValue().getUserId().equals(userId)){
                output.add(project.getValue());
            }
            if (output.isEmpty()) throw new NullPointerException();
        }
        return output;
    }


    @Override
    public void remove(final String id, final String userId) {
        final Iterator<Map.Entry<String, Project>> entryIterator = entities.entrySet().iterator();
        while (entryIterator.hasNext()) {
            Map.Entry<String, Project> projectEntry = entryIterator.next();
            if (projectEntry.getKey().equals(id) && projectEntry.getValue().getUserId().equals(userId)) {
                entryIterator.remove();
                break;
            }
        }
    }

    @Override
    public void removeAll(final String userId) {
        final Iterator<Map.Entry<String, Project>> entryIterator = entities.entrySet().iterator();
        while (entryIterator.hasNext()) {
            Map.Entry<String, Project> projectEntry = entryIterator.next();
            if (projectEntry.getValue().getUserId().equals(userId)) {
                entryIterator.remove();
            }
        }
    }
}
