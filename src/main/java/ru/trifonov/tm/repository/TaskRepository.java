package ru.trifonov.tm.repository;

import ru.trifonov.tm.api.ITaskRepository;
import ru.trifonov.tm.entity.Task;
import java.util.*;

public final class TaskRepository extends AbstractRepository<Task> implements ITaskRepository {
    private final Iterator<Map.Entry<String, Task>> entryIterator = entities.entrySet().iterator();

    @Override
    public void persist(final Task task) {
        entities.put(task.getId(), task);
    }

    @Override
    public void merge(final Task task) {
        if (entities.containsKey(task.getId())) {
            update(task.getName(), task.getId(), task.getProjectId(), task.getUserId(), task.getDescription(), task.getBeginDate(), task.getEndDate());
        } else
            insert(task.getName(), task.getId(), task.getProjectId(), task.getUserId(), task.getDescription(), task.getBeginDate(), task.getEndDate());
    }

    @Override
    public void insert(final String name, final String id, final String projectId, final String userId, final String description, final Date beginDate, final Date endDate) {
        final Task task = new Task(name, id, projectId, userId, description, beginDate, endDate);
        entities.put(id, task);
    }

    @Override
    public void update(final String name, final String id, final String projectId, final String userId, final String description, final Date beginDate, final Date endDate) {
        final Task task = new Task(name, id, projectId, userId, description, beginDate, endDate);
        entities.put(id, task);
    }

    @Override
    public Task findOne(final String id, final String userId) {
        Task task = entities.get(id);
        if (task == null) throw new NullPointerException();
        if (!task.getUserId().equals(userId)) throw new IllegalStateException();
        return task;
    }

    @Override
    public Collection<Task> findAll(final String projectId, final String userId) {
        final Collection<Task> output = new ArrayList<>();
        for (Map.Entry<String, Task> task : entities.entrySet()) {
            if (task.getValue().getProjectId().equals(projectId)) {
                output.add(task.getValue());
            }
            if (output.isEmpty()) throw new NullPointerException();
        }
        return output;
    }

    @Override
    public void remove(final String id, final String userId) {
        while (entryIterator.hasNext()) {
            Map.Entry<String, Task> projectEntry = entryIterator.next();
            if (projectEntry.getKey().equals(id) && projectEntry.getValue().getUserId().equals(userId)) {
                entryIterator.remove();
                break;
            }
        }
    }

    @Override
    public void removeAllOfProject(final String projectId, final String userId) {
        while (entryIterator.hasNext()) {
            Map.Entry<String, Task> projectEntry = entryIterator.next();
            if (projectEntry.getValue().getProjectId().equals(projectId) && projectEntry.getValue().getUserId().equals(userId)) {
                entryIterator.remove();
            }
        }
    }

    @Override
    public void removeAllOfUser(final String userId) {
        while (entryIterator.hasNext()) {
            Map.Entry<String, Task> projectEntry = entryIterator.next();
            if (projectEntry.getValue().getUserId().equals(userId)) {
                entryIterator.remove();
            }
        }
    }
}