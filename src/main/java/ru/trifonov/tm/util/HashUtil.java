package ru.trifonov.tm.util;

import java.security.MessageDigest;

public class HashUtil {
    public static String getHashMD5(String password) throws Exception{
        final byte[] bytesPassword = password.getBytes("UTF-8");
        return new String(MessageDigest.getInstance("MD5").digest(bytesPassword));
    }
}
