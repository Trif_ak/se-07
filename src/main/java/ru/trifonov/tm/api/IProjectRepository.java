package ru.trifonov.tm.api;

import ru.trifonov.tm.entity.*;

import java.util.Collection;
import java.util.Date;

public interface IProjectRepository {
    void merge(Project project);
    void persist(Project project);
    void insert(String name, String id, String userId, String description, Date beginDate, Date endDate);
    void update(String name, String id, String userId, String description, Date beginDate, Date endDate);
    Project findOne(String id, String userId) throws Exception;
    Collection<Project> findAll(String userId) throws Exception;
    void remove(String id, String userId);
    void removeAll(String userId);
}
