package ru.trifonov.tm.api;

import ru.trifonov.tm.entity.Task;

import java.util.Collection;
import java.util.Date;

public interface ITaskRepository {
    void merge(Task task);
    void persist(Task task);
    void insert(String name, String id, String projectId, String userId, String description, Date beginDate, Date endDate);
    void update(String name, String id, String projectId, String userId, String description, Date beginDate, Date endDate);
    Collection<Task> findAll(String projectId, String userId) throws Exception;
    Task findOne(String id, String userId) throws Exception;
    void remove(String id, String userId);
    void removeAllOfProject(String projectId, String userId);
    void removeAllOfUser(String userId);
}
