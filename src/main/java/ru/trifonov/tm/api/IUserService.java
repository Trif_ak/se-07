package ru.trifonov.tm.api;

import ru.trifonov.tm.entity.User;
import ru.trifonov.tm.enumerate.RoleType;

import java.util.Collection;

public interface IUserService {
    User authorizationUser(String login, String password) throws Exception;
    String registrationUser(String login, String password) throws Exception;
    String registrationAdmin(String login, String password) throws Exception;
    void update(String id, String login, String password, RoleType roleType) throws Exception;
    Collection<User> findAll() throws Exception;
    User findOne(String id) throws Exception;
    void removeOne(String id);
    void removeAll();
}
