package ru.trifonov.tm.service;

import java.text.SimpleDateFormat;

abstract class AbstractService {
    final SimpleDateFormat dateFormat = new SimpleDateFormat("DD.MM.YYYY");
}
