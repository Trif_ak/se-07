package ru.trifonov.tm.service;

import ru.trifonov.tm.api.IUserRepository;
import ru.trifonov.tm.api.IUserService;
import ru.trifonov.tm.entity.User;
import ru.trifonov.tm.enumerate.RoleType;
import ru.trifonov.tm.util.HashUtil;
import ru.trifonov.tm.util.IdUtil;

import java.security.MessageDigest;
import java.util.Collection;
import java.util.UUID;

public final class UserService extends AbstractService implements IUserService {
    private IUserRepository userRepository;

    public UserService(IUserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public User authorizationUser(final String login, final String password) throws Exception {
        if (login == null || login.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (password == null || password.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        return userRepository.findPassword(userRepository.findLogin(login), HashUtil.getHashMD5(password));
    }

    @Override
    public String registrationUser(final String login, final String password) throws Exception {
        if (login == null || login.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (password == null || password.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (userRepository.findLogin(login) != null) throw new NullPointerException("Enter correct data");
        User user = new User(IdUtil.getUUID(), login, HashUtil.getHashMD5(password), RoleType.REGULAR_USER);
        userRepository.persist(user);

        return user.getLogin();
    }

    @Override
    public String registrationAdmin(final String login, final String password) throws Exception {
        if (login == null || login.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (password == null || password.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (userRepository.findLogin(login) != null) throw new NullPointerException("Enter correct data");
        User user = new User(IdUtil.getUUID(), login, HashUtil.getHashMD5(password), RoleType.ADMIN);
        userRepository.persist(user);

        return user.getLogin();
    }

    @Override
    public void update(final String id, final String login, final String password, final RoleType roleType) throws Exception {
        if (id == null || id.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (login == null || login.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (password == null || password.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (roleType == null) throw new NullPointerException("Enter correct data");

        userRepository.update(id, login, HashUtil.getHashMD5(password), roleType);
    }
    @Override
    public Collection<User> findAll() throws Exception {
        return userRepository.findAll();
    }

    @Override
    public User findOne(final String id) throws Exception {
        if (id == null || id.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        return userRepository.findOne(id);
    }

    @Override
    public void removeOne(final String id) {
        if (id == null || id.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        userRepository.removeOne(id);
    }

    @Override
    public void removeAll() {
        userRepository.removeAll();
    }
}
