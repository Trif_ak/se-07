package ru.trifonov.tm.service;

import ru.trifonov.tm.api.IProjectRepository;
import ru.trifonov.tm.api.IProjectService;
import ru.trifonov.tm.entity.Project;
import ru.trifonov.tm.util.IdUtil;

import java.text.ParseException;
import java.util.Collection;

public final class ProjectService extends AbstractService implements IProjectService {
    private IProjectRepository projectRepository;

    public ProjectService(final IProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    public void persist(final String name, final String userId, final String description, final String beginDate, final String endDate) throws Exception {
        if (name == null || name.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (userId == null || userId.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (description == null || description.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (beginDate == null || beginDate.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (endDate == null || endDate.trim().isEmpty()) throw new NullPointerException("Enter correct data");

        final Project project = new Project(name, IdUtil.getUUID(), userId, description, dateFormat.parse(beginDate), dateFormat.parse(endDate));
        projectRepository.persist(project);
    }

    @Override
    public void merge(final String name, final String id, final String userId, final String description, final String beginDate, final String endDate) throws Exception {
        if (name == null || name.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (id == null || id.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (userId == null || userId.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (description == null || description.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (beginDate == null || beginDate.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (endDate == null || endDate.trim().isEmpty()) throw new NullPointerException("Enter correct data");

        final Project project = new Project(name, IdUtil.getUUID(), userId, description, dateFormat.parse(beginDate), dateFormat.parse(endDate));
        projectRepository.merge(project);
    }

    @Override
    public Project findOne(final String id, final String userId) throws Exception {
        if (id == null || id.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (userId == null || userId.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        return projectRepository.findOne(id, userId);
    }

    @Override
    public Collection<Project> findAll(final String userId) throws Exception {
        if (userId == null || userId.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        return projectRepository.findAll(userId);
    }

    @Override
    public void removeOne(final String id, final String userId) {
        if (id == null || id.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (userId == null || userId.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        projectRepository.remove(id, userId);
    }

    @Override
    public void removeAll(final String userId) {
        if (userId == null || userId.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        projectRepository.removeAll(userId);
    }
}
