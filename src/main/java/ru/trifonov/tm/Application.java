package ru.trifonov.tm;

import ru.trifonov.tm.command.project.*;
import ru.trifonov.tm.command.task.*;
import ru.trifonov.tm.command.user.*;
import ru.trifonov.tm.command.system.*;
import ru.trifonov.tm.bootstrap.*;


public final class Application {
    public final static Class[] COMMANDS = {
            HelpCommand.class, AboutCommand.class, ExitCommand.class,
            ProjectFindAllCommand.class, ProjectFindOneCommand.class,
            ProjectPersistCommand.class, ProjectMergeCommand.class,
            ProjectRemoveAllCommand.class, ProjectRemoveOneCommand.class,

            TaskFindAllCommand.class, TaskFindOneCommand.class,
            TaskPersistCommand.class, TaskMergeCommand.class,
            TaskRemoveAllCommand.class, TaskRemoveOneCommand.class,

            UserAuthorizationCommand.class, UserRegistrationCommand.class,
            UserUpdatePasswordCommand.class, UserEndOfSessionCommand.class,
            UserFindOneCommand.class, UserEditProfileCommand.class,
            UserFindAllCommand.class, UserRemoveAllCommand.class,
            UserRemoveOneCommand.class, UserRegistrationAdminCommand.class
    };

    public static void main(String[] args) throws Exception {
        final Bootstrap bootstrap = new Bootstrap();
        bootstrap.init(COMMANDS);
    }
}